const rp = require('request-promise');

const baseUrl = 'https://a.4cdn.org';
const chanPull = {};
const requestOptions = {
  json: true,
  headers: {
    'if-modified-since': (new Date()).toUTCString(),
  },
};


chanPull.board = (board) => {
  const boardPull = {};
  boardPull.board = board;

  // Gathers all the Titles from a board, if their ID isn't already on a Table.
  boardPull.titles = id => new Promise((resolve) => {
    const uri = [baseUrl, board, 'catalog.json'].join('/');
    const result = [];
    const newId = id;
    rp(uri, requestOptions)
            .then((body) => {
              for (let i = 0; i < body.length; i += 1) {
                for (let j = 0; j < body[i].threads.length; j += 1) {
                  if (body[i].threads[j].sub !== null && body[i].threads[j].sub !== undefined) {
                    if (!newId[body[i].threads[j].no]) {
                      result.push([body[i].threads[j].no, body[i].threads[j].sub]);
                      newId[body[i].threads[j].no] = 1;
                    }
                  }
                }
              }
            });
    setTimeout(() => { resolve(result); }, 1000);
  });

  // Grabs all the thread ID's from a board
  boardPull.threads = new Promise((resolve) => {
    const uri = [baseUrl, board, 'threads.json'].join('/');

    rp(uri, requestOptions)
            .then((body) => {
              resolve(body);
            });
	return chanPull;
  });

 // Cycles through every thread on a Board, pulling relevant "tags"
  function getTags(i, arr, uri, result, start, id, tag) {
    let count = i;
    let newUri = uri;
    const newId = id;
    // 4chan's public API requests that you
    // try not to pull more than once per second
    // so we do a lazy timer loop
    return new Promise((resolve) => {
      if (start === 0) {
        setTimeout(() => {
          resolve();
        }, (i * 1000) + 1000);
      }

      setTimeout(() => {
        count -= 1;
        if (count) {
          getTags(count, arr, newUri, result, 1, newId, tag);
        }
        console.log(count);
        newUri = [baseUrl, board, 'thread', `${arr[count]}.json`].join('/');
        rp(newUri, requestOptions).then((body) => {
          if (body !== undefined) {
            for (let j = 0; j < body.posts.length; j += 1) {
              if (body.posts[j][tag[0]] !== undefined) {
                // checking if the unique ID has been stored before 'no'
                if (!newId[body.posts[j].no]) {
                  // will rewrite or extend should I ever need more than 3 tags per post
                  if (tag.length === 1) {
                    result.push([body.posts[j].no, body.posts[j][tag[0]]]);
                    newId[body.posts[j].no] = 1;
                  } else if (tag.length === 2) {
                    result.push([body.posts[j].no, body.posts[j][tag[0]], body.posts[j][tag[1]]]);
                    newId[body.posts[j].no] = 1;
                  } else if (tag.length === 3) {
                    result.push([body.posts[j].no, body.posts[j][tag[0]], body.posts[j][tag[1]],
                      body.posts[j][tag[2]]]);
                    newId[body.posts[j].no] = 1;
                  }
                }
              }
            }
          }
        });
      }, 1000);
    });
  }
  // This is how the above loop get's started
  boardPull.getTagContent = (arr, id, tag) => new Promise((resolve) => {
    const uri = [];
    const result = [];
    getTags(arr.length, arr, uri, result, 0, id, tag).then(() => {
      resolve(result);
    });
  });

  return boardPull;
};

module.exports = chanPull;

