const chanPull = require('./chan-pull');
const mysql = require('mysql');
const CronJob = require('cron').CronJob;

const pol = chanPull.board('pol');
const g = chanPull.board('g');
const fit = chanPull.board('fit');
const con = mysql.createConnection({
  host: 'xxxx',
  user: 'xxxx',
  password: 'xxxx',
  database: 'xxxx',
});

// Creates an Object to feed into the data grabbing functions.
class ChanArg {
  constructor(board, table, col, tag, pattern) {
    this.board = board; // obj - const x = chanPull.board('x');
    this.table = table; // string - sql table name
    this.col = col; // array - table column/columns
    this.tag = tag; // array - tag you're looking for
    this.pattern = pattern; // regex - if you want to filter the tag against something.
  }
}

//Clean up The lazy Regex
const patterns = {
  g: /\bc \b|\bc$|python|javascript|\bjs$|\bjs |\bjava$|\bjava |c\+\+|haskell|lisp|swift|php|ruby|perl|bash|amd|intel|amds|intels|amd's|intel's/gi,
  fit: /Texas Method|Madcow|PHUL|\bPPL |\bPPL$|\bSS$|\bSS |GSLP|Reg Park|ICF|\bSL |\bSL$|Starting Strength|StartingStrength|GreySkull|GraySkull|StrongLifts|\bULPP|\bULPP$ /gi,
};

const argsPol = new ChanArg(pol, 'polinfo', ['countryname', 'filename'], ['country_name', 'filename'], false);
const argsFit = new ChanArg(fit, 'fitinfo', ['comment'], ['com'], patterns.fit);
const argsG = new ChanArg(g, 'ginfo', ['comment'], ['com'], patterns.g);
const argsTitleG = new ChanArg(g, 'gtitles');
const argsTitleFit = new ChanArg(fit, 'fittitles');
const argsTitlePol = new ChanArg(pol, 'poltitles');


// Returns an Array containing all the Thread IDs
const getThreads = (array) => {
  const result = [];
  for (let i = 0; i < array.length; i += 1) {
    for (let j = 0; j < array[i].threads.length; j += 1) {
      result.push(array[i].threads[j].no);
    }
  }
  return result;
};

// Inserts Data Into a Table. "Arr", new values, "Col", columns
const insertData = (arr, table, col) => {
  let result = {};
  for (let i = 0; i < arr.length; i += 1) {
    if (col.length === 1) {
      result = `INSERT INTO  \`${table}\` (\`id\`, \`${col[0]}\`) VALUES ('${arr[i][0]}', '${arr[i][1]}')`;
    } else if (col.length === 2) {
      result = `INSERT INTO  \`${table}\` (\`id\`, \`${col[0]}\`, \`${col[1]}\`) VALUES ('${arr[i][0]}', '${arr[i][1]}', '${arr[i][2]}')`;
    }
    con.query(result, (err) => {
      if (err) console.log(err);
    });
  }
};

// Gets all the Unique ID's from pre existing Tables
const getId = table => new Promise((resolve) => {
  const idRow = {};
  const sql = `SELECT \`id\` FROM \`${table}\``;
  con.query(sql, (err, rows) => {
    if (err) throw err;
    for (let i = 0; i < rows.length; i += 1) {
      idRow[rows[i].id] = 1;
      if (i + 1 === rows.length) resolve(idRow);
    }
  });
});

// Gather's all active Thread Titles and Inserts It into relevent Table if they're not present
const getTitles = (arg) => {
  getId(arg.table).then((x) => {
    const id = x;
    arg.board.titles(id).then((result) => {
      insertData(result, arg.table, ['title']);
    });
  })
    .catch((err) => { console.log(err); });
};

// Used against comments to get rid of quote chains and garbage
const commentSort = (arr, reg) => new Promise((resolve) => {
  const result = [];
  for (let i = 0; i < arr.length; i += 1) {
    let str = arr[i][1].replace(/<a [\s\S]*?<\/a>|<[\s\S]*?>|&[\s\S]*?;/gi, '');
    str = str.match(reg);
    if (str === null) {
      str = 'null';
    } else {
      str = str.join(',');
    }
    result.push([arr[i][0], str]);
  }
  if (result.length === arr.length) resolve(result);
});

// Cycles through every active thread on board, scraping for relevant tags defined by Arguments
const getChanData = (arg) => {
  getId(arg.table).then((x) => {
    const id = x;
    arg.board.threads.then((threads) => {
      arg.board.getTagContent(getThreads(threads), id, arg.tag)
            .then((result2) => {
              if (arg.pattern === false) {
                insertData(result2, arg.table, arg.col);
              } else {
                commentSort(result2, arg.pattern)
                  .then((result3) => {
                    insertData(result3, arg.table, arg.col);
                  });
              }
            });
    });
  })
        .catch((err) => { console.log(err); });
};

// Makes a new Cron Job for getChanData Function, takes Cron Format Time, and ChanArg Class
function updateChanInfo(time, arg) {
  return new CronJob(time, () => {
    getChanData(arg);
  }, null, true, 'America/Los_Angeles');
}

// Makes a new Cron Job for getTitltes Function, takes Cron Format Time, and ChanArg Class
function updateChanTitles(time, arg) {
  return new CronJob(time, () => {
    getTitles(arg);
  }, null, true, 'America/Los_Angeles');
}

con.connect((err) => {
  if (err) console.log(err);
  console.log('Connected!');

  updateChanTitles('0 */1 * * *', argsTitleFit);
  updateChanTitles('2 */1 * * *', argsTitleG);
  updateChanTitles('5 */1 * * *', argsTitlePol);
  updateChanInfo('10 */1 * * *', argsFit);
  updateChanInfo('20 */1 * * *', argsG);
  updateChanInfo('40 */1 * * *', argsPol);
});
